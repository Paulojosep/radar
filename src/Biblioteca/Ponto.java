/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Biblioteca;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import static javafx.scene.paint.Color.color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class Ponto extends JPanel {
    
    public static final int CANVAS_WIDTH = 640;
    public static final int CANVAS_HEIGHT = 480;
    public static final String TITLE = "Affine Transform Demo";
    
    int[] polygonXs = {-20, 0, +20,0};
    int[] polygonYs = {20, 10, 20, -20};
    Shape shape = new Polygon(polygonXs, polygonYs, polygonXs.length);
    double x = 50.0, y = 50.0;
    
    public Ponto(){
        setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        setBackground(Color.WHITE);
        Graphics2D g2d = (Graphics2D)g;
        
        AffineTransform saveTransformDemo = g2d.getTransform();
        AffineTransform identity = new AffineTransform();
        g2d.setTransform(identity);
        g2d.setColor(Color.GREEN);
        g2d.fill(shape);
        g2d.translate(x, y);
        g2d.scale(1.2, 1.2);
        g2d.fill(shape);
        
        for (int i = 0;i < 5; ++i){
            g2d.translate(50.0, 5.0);
            g2d.setColor(Color.BLUE);
            g2d.fill(shape);
            g2d.rotate(Math.toRadians(15.0));
            g2d.setColor(Color.RED);
            g2d.fill(shape);
        }
        
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run() {
                JFrame frame = new JFrame(TITLE);
                frame.setContentPane(new Ponto());
                frame.pack();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        
        });
    }
    
}
