/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Biblioteca;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class Main extends JFrame  {
    
    private static int POINT_SIZE =5;
    private static int LINE_SIZE = 3;
    private static int WINDOW_WIDTH = 1000;
    private static int WINDOW_HEIGHT = 750;
    private static Color colorPixel = Color.GREEN;
    
    public Main(){
       initComponente();
    }
    
    /*public void paint(Graphics g){
        g.setColor(Color.black);
        g.fillRect(0, 0, 1000, 750);
        g.setColor(Color.green);
        
        //Desenho do Circulo
        g.drawArc(400, 300, 200, 200, 200, 300);
        g.drawArc(400, 300, 200, 200, 100, 200);
        g.drawArc(400, 300, 200, 200, 200, 200);
        g.drawArc(350, 250, 300, 300, 200, 300);
        g.drawArc(350, 250, 300, 300, 300, 300);
        g.drawArc(300, 200, 400, 400, 400, 400);
        g.drawArc(250, 150, 500, 500, 500, 500);
        g.drawArc(200, 100, 600, 600, 600, 600);
        
        //Desenho das linhas paralelas
        g.drawLine(150, 100, 850, 700);
        g.drawLine(150, 700, 850, 100);
    }
    
    public static void main(String[] args) {
        new Menu();
    }*/
    
    
    

    
    // Metod de desenho primitivo
    @Override
    public void paint(Graphics g) {
        
        Graphics2D g2 = (Graphics2D) g;
        
        //g.drawLine(200,200, 180 , 200);
        
        super.paintComponents(g2);
        //g.fillOval(100, 100, 3, 3);
        g.fillRect(0, 0, 1000, 750);
        //putPixel(g2,-100,-100,Color.RED);
        putPixel(g2,0,0,Color.GREEN);
        
        ////////////CIRCULOS////////////////////////
        drawCircleImage(g2,0,0,350,1000,Color.GREEN);
        drawCircleImage(g2,0,0,310,2000,Color.GREEN);
        drawCircleImage(g2,0,0,255,2000,Color.GREEN);
        drawCircleImage(g2,0,0,205,1000,Color.GREEN);
        drawCircleImage(g2,0,0,150,1000,Color.GREEN);
        drawCircleImage(g2,0,0,100,1000,Color.GREEN);
        drawCircleImage(g2,0,0,50,1000,Color.GREEN);
        
        /////////////RETAS////////////////////////////
        drawLine(g2,0,0,250,250,Color.GREEN);           //Diagonal para direita
        drawLine(g2,0,0,-250,-250,Color.GREEN);         //Diagonal para esquerda
        drawLine(g2,0,0,350,0,Color.GREEN);             //Esquerda para direita
        drawLine2(g2,-350,0,0,0,Color.RED);             //Direita para esquerda
        
        /*BufferedImage imagem = new BufferedImage(WINDOW_WIDTH, WINDOW_HEIGHT, BufferedImage.TYPE_INT_BGR);
        
        drawLine(imagem,Color.GREEN, 200,200, 180 , 200);
        drawLine(imagem,Color.GREEN, 20,200, 180 , 150);
        drawLine2(imagem,0,0,100,200,Color.GREEN);
        drawLine3(imagem,0,0,200,200,Color.GREEN);*/
        //g.drawLine(100, 100, 200, 250);  
    }
    
    private void initComponente(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    } 
    
    public void putPixel(Graphics g, int x, int y ,Color c){
        y = -y;
        g.setColor(c);
        x += WINDOW_WIDTH/2;
        y += WINDOW_HEIGHT/2;
        g.fillOval(x, y,POINT_SIZE, POINT_SIZE);
    }

    // Linha Horizontal
    /*public void drawLine(BufferedImage imagem,Color c, int x1, int y1, int width, int height) {
        int x2 = x1 + width;
        int y2 = y1 + height;
        int deltax = Math.abs(x2 - x1);
        int deltay = Math.abs(y2 - y1);
        int y = y1;
        int error = 0;
        for (int x = x1; x < x2; x++) {
            imagem.setRGB(x, y1, c.getRGB());
            error = error + deltay;
            if (2 * error >= deltax) {
                y = y + 1;
                error = error - deltax;
            }
        }
       //repaint();
    }*/
    
    // Algoritimo de Bresenham
    public void drawLine(Graphics g,int x1, int y1, int x2, int y2, Color cor){
        int X;
        int Y;
        int DeltX = x2 - x1;
        int DeltY = y2 - y1;
        int P = 2*(DeltY - DeltX);
        int P2 = 2*DeltY;
        int xy2 = 2*(DeltY - DeltX);
        
        if(x1 > x2){
            X = x2;
            Y = y2;
            x2 = x1;
        }else{
            X = x1;
            Y = y1;
            x2 = x2;
        }
        
        //imagem.setRGB(X, Y, cor.getRGB());
        putPixel(g,X,Y,Color.GREEN);
        
        while(X < x2){
            X+=1;
            
            if(P < 0){
                P+=P2;
            }else{
                Y+=1;
                P+=xy2;
            }
            //imagem.setRGB(X, Y, cor.getRGB());
            putPixel(g,X,Y,Color.GREEN);
        }
        return;
    }
    
    //Algoritimo de DDA
        public void drawLine2(Graphics g,int x0, int y0, int x1, int y1, Color cor){
            int Xi,Yi;
            int S;
            int deltaX = x1 - x0;
            int deltaY = y1 - y0;
            int X = x0;
            int Y = y0;
            
            if(Math.abs(deltaX) > Math.abs(deltaY))
                S = Math.abs(deltaX);
            else
                S = Math.abs(deltaY);
            Xi = deltaX/S;
            Yi = deltaY/S;
            putPixel(g,X,Y,Color.GREEN);
            
            while(X != 0){
                X+=Xi;
                Y+=Yi;
                putPixel(g,X,Y,Color.GREEN);
            } 
        }
    
    
    public void drawCircleImage(Graphics g,int posicaoCentroX,int posicaoCentroY,int raio,int quantidadeDePontos,Color cor){
        double distaciaEntrePontos = 2 * Math.PI / quantidadeDePontos;
        
        for(int i = 0; i<quantidadeDePontos;i++){
            double cos = Math.cos(i* distaciaEntrePontos);
            double sin = Math.sin(i * distaciaEntrePontos );
            
            int x = (int) (cos * raio + posicaoCentroX);
            int y = (int) (sin * raio + posicaoCentroY);
            
            //imagem.setRGB(x,y,cor.getRGB());   
            putPixel(g,x,y,Color.GREEN);
        }
        //repaint();
    }
    
    public void triangulo(BufferedImage imagem,int x,int y, double cos){
        
    }
    
    
    public static void main(String[] args) {

        new Main();
        
    }

}
